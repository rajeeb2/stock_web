import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StockDetailsService } from 'src/app/service/stock-details.service'


@Component({
  selector: 'app-stock-information',
  templateUrl: './stock-information.component.html',
  styleUrls: ['./stock-information.component.scss']
})
export class StockInformationComponent implements OnInit {
  stockList: any = [];
  keyword = 'companyName';
  stockDetails: any = [];
  showDetails = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private service: StockDetailsService
  ) { }


  ngOnInit(): void {
    this.getStockList()
  }


  getStockList() {
    this.service.getStockList().subscribe((result: any) => {
      if (result.status) {
       this.stockList = result.data
      }
    }, error => {
      console.log('getStockList err:', error);
    });
  }
  selectEvent(item: any) {
    this.stockDetails = item;
    this.showDetails = true;
    // do something with selected item
  }

  onChangeSearch(val: string) {
    this.showDetails = false;
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  onFocused(e: any){
    // do something when input is focused
  }
}



