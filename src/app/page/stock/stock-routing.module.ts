import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockInformationComponent } from './stock-information/stock-information.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: StockInformationComponent },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockRoutingModule { }
