import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configuration } from '../../configuration';

@Injectable({
  providedIn: 'root'
})
export class StockDetailsService {
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  
  constructor(private http: HttpClient, public config: Configuration) { }

  getStockList() {
    return this.http.post(this.config.getStockList, this.httpOptions);
  }
}
