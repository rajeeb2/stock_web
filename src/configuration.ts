import { environment } from 'src/environments/environment';
export class Configuration {
    public apiURL = environment.siteURL + '/api';
    public getStockList = this.apiURL + '/stock/getStockList';
}
